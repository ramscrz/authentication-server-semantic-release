Authentication Server Module

# [2.0.0](https://gitlab.com/ramscrz/authentication-server-semantic-release/compare/v1.0.0...v2.0.0) (2021-08-04)


### Bug Fixes

* Fix broken integration tests and CI silent fail ([f89cd9c](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/f89cd9c84bfabdacf750306e26ee732a19bbab94))
* Fix linting issues and error when account data does not exist. ([29e5f83](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/29e5f833e8c829b18a4b0915e5c0e00768451789))
* **Configuration:** Fix the dependency to the correct Configuration module ([b995f21](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/b995f21aacb782477349de620ebf7eec55e18dde))


### Features

* Add AccountData model and move Account to models package ([c7b046c](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/c7b046cdcc314a03f4bebf57bbcf98a0497a993e))
* Add Pitaya handshake AccountData to database during BindS ([12fc496](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/12fc4961ba767cf250132301e604fe47d9f3609e))


### BREAKING CHANGES

*

# [2.0.0](https://gitlab.com/ramscrz/authentication-server-semantic-release/compare/v1.0.0...v2.0.0) (2021-08-04)


### Bug Fixes

* Fix broken integration tests and CI silent fail ([f89cd9c](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/f89cd9c84bfabdacf750306e26ee732a19bbab94))
* Fix linting issues and error when account data does not exist. ([29e5f83](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/29e5f833e8c829b18a4b0915e5c0e00768451789))
* **Configuration:** Fix the dependency to the correct Configuration module ([b995f21](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/b995f21aacb782477349de620ebf7eec55e18dde))


### Features

* Add AccountData model and move Account to models package ([c7b046c](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/c7b046cdcc314a03f4bebf57bbcf98a0497a993e))
* Add Pitaya handshake AccountData to database during BindS ([12fc496](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/12fc4961ba767cf250132301e604fe47d9f3609e))


### BREAKING CHANGES

*

# 1.0.0 (2021-08-04)


### Features

* Implement the standard Inventory inside the Store module ([0506129](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/0506129c5f2a09ee419a6e123ec4c5d3ef1c0d6a))

# 1.0.0 (2021-08-04)


### Features

* Implement the standard Inventory inside the Store module ([0506129](https://gitlab.com/ramscrz/authentication-server-semantic-release/commit/0506129c5f2a09ee419a6e123ec4c5d3ef1c0d6a))
